package com.elasticsearch.spring.springelasticsearch.domain;

public class EopLogJtEn {
    private String id;
    private String apiId="9193";
    private String apiName="广东_BSS_CRM_FUNC009_登录查询组件_短厅掌厅专用";
    private String appId="1216";
    private Integer backendResponseCode=200;
    private Integer backendResponseTime=179;
    private String basePath="/crm-ws/crm-svc/FUNC009";
    private String businessResponseCode=null;
    private String clientIP="132.120.64.210";
    private String component="6001010001";
    private Integer gatewayExecTime=44;
    private String gatewayIP="eop-prod-crm-api-center";
    private String gatewayName="广东EOP网关";
    private Integer gatewayResponseCode=200;
    private Integer gatewayResponseTime=0;
    private String host="eop-prod-crm-api-center";
    private String method="POST";
    private String packageId="00008524";
    private String packageName="广东_BSS_CRM_FUNC009_登录查询组件_短厅掌厅专用组合包";
    private Long requestForwardTime=null;
    private String requestId="7315851882732474447";
    private Long requestReceivedTime=1585188273295L;
    private Double requestSize=2.53;
    private Long responseForwardTime=1585188273251L;
    private Long responseReceivedTime=1585188273474L;
    private Double responseSize=3.64;
    private String targetBasePath="/crm-svc/services";
    private String targetHost="132.121.208.1:8100";
    private String targetScheme="http";
    private Long timestamp=	1585188273474L;
    private Integer totalTime=223;
    private String uri="/gd_eop/crm-ws/crm-svc/FUNC009/ESBService";
    private String userAgent="Jakarta Commons-HttpClient/3.0";
    private String requestUrl="/gd_eop/crm-ws/crm-svc/FUNC009/ESBService";

    @Override
    public String toString() {
        return "EopLogJtEn{" +
                "id='" + id + '\'' +
                ", apiId='" + apiId + '\'' +
                ", apiName='" + apiName + '\'' +
                ", appId='" + appId + '\'' +
                ", backendResponseCode=" + backendResponseCode +
                ", backendResponseTime=" + backendResponseTime +
                ", basePath='" + basePath + '\'' +
                ", businessResponseCode='" + businessResponseCode + '\'' +
                ", clientIP='" + clientIP + '\'' +
                ", component='" + component + '\'' +
                ", gatewayExecTime=" + gatewayExecTime +
                ", gatewayIP='" + gatewayIP + '\'' +
                ", gatewayName='" + gatewayName + '\'' +
                ", gatewayResponseCode=" + gatewayResponseCode +
                ", gatewayResponseTime=" + gatewayResponseTime +
                ", host='" + host + '\'' +
                ", method='" + method + '\'' +
                ", packageId='" + packageId + '\'' +
                ", packageName='" + packageName + '\'' +
                ", requestForwardTime=" + requestForwardTime +
                ", requestId='" + requestId + '\'' +
                ", requestReceivedTime=" + requestReceivedTime +
                ", requestSize=" + requestSize +
                ", responseForwardTime=" + responseForwardTime +
                ", responseReceivedTime=" + responseReceivedTime +
                ", responseSize=" + responseSize +
                ", targetBasePath='" + targetBasePath + '\'' +
                ", targetHost='" + targetHost + '\'' +
                ", targetScheme='" + targetScheme + '\'' +
                ", timestamp=" + timestamp +
                ", totalTime=" + totalTime +
                ", uri='" + uri + '\'' +
                ", userAgent='" + userAgent + '\'' +
                ", requestUrl='" + requestUrl + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public Integer getBackendResponseCode() {
        return backendResponseCode;
    }

    public void setBackendResponseCode(Integer backendResponseCode) {
        this.backendResponseCode = backendResponseCode;
    }

    public Integer getBackendResponseTime() {
        return backendResponseTime;
    }

    public void setBackendResponseTime(Integer backendResponseTime) {
        this.backendResponseTime = backendResponseTime;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public String getBusinessResponseCode() {
        return businessResponseCode;
    }

    public void setBusinessResponseCode(String businessResponseCode) {
        this.businessResponseCode = businessResponseCode;
    }

    public String getClientIP() {
        return clientIP;
    }

    public void setClientIP(String clientIP) {
        this.clientIP = clientIP;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public Integer getGatewayExecTime() {
        return gatewayExecTime;
    }

    public void setGatewayExecTime(Integer gatewayExecTime) {
        this.gatewayExecTime = gatewayExecTime;
    }

    public String getGatewayIP() {
        return gatewayIP;
    }

    public void setGatewayIP(String gatewayIP) {
        this.gatewayIP = gatewayIP;
    }

    public String getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName) {
        this.gatewayName = gatewayName;
    }

    public Integer getGatewayResponseCode() {
        return gatewayResponseCode;
    }

    public void setGatewayResponseCode(Integer gatewayResponseCode) {
        this.gatewayResponseCode = gatewayResponseCode;
    }

    public Integer getGatewayResponseTime() {
        return gatewayResponseTime;
    }

    public void setGatewayResponseTime(Integer gatewayResponseTime) {
        this.gatewayResponseTime = gatewayResponseTime;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Long getRequestForwardTime() {
        return requestForwardTime;
    }

    public void setRequestForwardTime(Long requestForwardTime) {
        this.requestForwardTime = requestForwardTime;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Long getRequestReceivedTime() {
        return requestReceivedTime;
    }

    public void setRequestReceivedTime(Long requestReceivedTime) {
        this.requestReceivedTime = requestReceivedTime;
    }

    public Double getRequestSize() {
        return requestSize;
    }

    public void setRequestSize(Double requestSize) {
        this.requestSize = requestSize;
    }

    public Long getResponseForwardTime() {
        return responseForwardTime;
    }

    public void setResponseForwardTime(Long responseForwardTime) {
        this.responseForwardTime = responseForwardTime;
    }

    public Long getResponseReceivedTime() {
        return responseReceivedTime;
    }

    public void setResponseReceivedTime(Long responseReceivedTime) {
        this.responseReceivedTime = responseReceivedTime;
    }

    public Double getResponseSize() {
        return responseSize;
    }

    public void setResponseSize(Double responseSize) {
        this.responseSize = responseSize;
    }

    public String getTargetBasePath() {
        return targetBasePath;
    }

    public void setTargetBasePath(String targetBasePath) {
        this.targetBasePath = targetBasePath;
    }

    public String getTargetHost() {
        return targetHost;
    }

    public void setTargetHost(String targetHost) {
        this.targetHost = targetHost;
    }

    public String getTargetScheme() {
        return targetScheme;
    }

    public void setTargetScheme(String targetScheme) {
        this.targetScheme = targetScheme;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Integer totalTime) {
        this.totalTime = totalTime;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }
}
