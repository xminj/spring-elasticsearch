package com.elasticsearch.spring.springelasticsearch.domain;

import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "first-index",type = "string")
public class VersionDomain {
    private String number;
    private String build_flavor;
    private String build_type;
    private String build_hash;
    private String build_date;
    private String build_snapshot;
    private String lucene_version;
    private String minimum_wire_compatibility_version;
    private String minimum_index_compatibility_version;

    public String getNumber() {
        return number;
    }

    public VersionDomain setNumber(String number) {
        this.number = number;
        return this;
    }

    public String getBuild_flavor() {
        return build_flavor;
    }

    public VersionDomain setBuild_flavor(String build_flavor) {
        this.build_flavor = build_flavor;
        return this;
    }

    public String getBuild_type() {
        return build_type;
    }

    public VersionDomain setBuild_type(String build_type) {
        this.build_type = build_type;
        return this;
    }

    public String getBuild_hash() {
        return build_hash;
    }

    public VersionDomain setBuild_hash(String build_hash) {
        this.build_hash = build_hash;
        return this;
    }

    public String getBuild_date() {
        return build_date;
    }

    public VersionDomain setBuild_date(String build_date) {
        this.build_date = build_date;
        return this;
    }

    public String getBuild_snapshot() {
        return build_snapshot;
    }

    public VersionDomain setBuild_snapshot(String build_snapshot) {
        this.build_snapshot = build_snapshot;
        return this;
    }

    public String getLucene_version() {
        return lucene_version;
    }

    public VersionDomain setLucene_version(String lucene_version) {
        this.lucene_version = lucene_version;
        return this;
    }

    public String getMinimum_wire_compatibility_version() {
        return minimum_wire_compatibility_version;
    }

    public VersionDomain setMinimum_wire_compatibility_version(String minimum_wire_compatibility_version) {
        this.minimum_wire_compatibility_version = minimum_wire_compatibility_version;
        return this;
    }

    public String getMinimum_index_compatibility_version() {
        return minimum_index_compatibility_version;
    }

    public VersionDomain setMinimum_index_compatibility_version(String minimum_index_compatibility_version) {
        this.minimum_index_compatibility_version = minimum_index_compatibility_version;
        return this;
    }
}
