package com.elasticsearch.spring.springelasticsearch.config;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Configuration
public class TransportClientConfig {

    @Bean
    public Client elasticsearchClient() throws UnknownHostException {
        Settings settings = Settings.builder()
                .put("cluster.name","118-25-54-125")           // cluster 集群名称
                //.put("client.transport.sniff",true)   // 启用嗅探
                //.put("client.transport.ignore_cluster_name",true)  // 0.19.4版本 忽略连接节点的集群名称验证
                //.put("client.transport.ping_timeout",50000)  // 等待节点发出ping响应的时间。默认为5 s。
                //.put("client.transport.nodes_sampler_interval",50000) // 采样/ ping列出和连接的节点的频率。 默认为5s
                .build();
        TransportClient client = new PreBuiltTransportClient(settings)
                .addTransportAddress(new TransportAddress(InetAddress.getByName("118.25.54.125"),9300));

        //IndicesAdminClient indices = client.admin().indices();
        return client;
    }

    @Bean(name = {"elasticsearchOperations","elasticsearchTemplate"})
    public ElasticsearchTemplate elasticsearchTemplate() throws UnknownHostException {
        return new ElasticsearchTemplate(elasticsearchClient());
    }
}
