package com.elasticsearch.spring.springelasticsearch.annotation;


import com.elasticsearch.spring.springelasticsearch.domain.EopLogJt;
import javassist.*;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.AttributeInfo;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import javassist.bytecode.annotation.MemberValue;
import javassist.bytecode.annotation.StringMemberValue;
import org.springframework.data.elasticsearch.annotations.Document;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.Map;

public class AnnotationFieldValue {

    /**
     * 设置注解中的字段值
     *
     * @param annotation 要修改的注解实例
     * @param fieldName  要修改的注解字段名
     * @param value      要设置的值
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    public static void setAnnotationValue(Annotation annotation, String fieldName, Object value) throws NoSuchFieldException, IllegalAccessException {
        InvocationHandler handler = Proxy.getInvocationHandler(annotation);
        Field hField = handler.getClass().getDeclaredField("memberValues");
        hField.setAccessible(true);
        Map memberValues = (Map) hField.get(handler);
        memberValues.put(fieldName, value);
        System.out.println(memberValues);

    }

    /**
     * @param indexName
     * @throws NotFoundException
     * @throws ClassNotFoundException
     * @throws CannotCompileException
     */
    public static  Class<?> documentIndexName(String indexName,String entityClassName) throws NotFoundException, CannotCompileException {
        ClassPool aDefault = ClassPool.getDefault();
        //aDefault.appendClassPath(new ClassClassPath(EopLogJt.class));

        entityClassName = EopLogJt.class.getName();
        CtClass ctClass = aDefault.get(entityClassName);

        ctClass.stopPruning(true);
        ctClass.defrost();

        ClassFile classFile = ctClass.getClassFile();


        ConstPool constPool = classFile.getConstPool();
        javassist.bytecode.annotation.Annotation documentAnnotation =
                new javassist.bytecode.annotation.Annotation(Document.class.getName(), constPool);
        documentAnnotation.addMemberValue("indexName", new StringMemberValue(indexName, constPool));

        AnnotationsAttribute attribute = new AnnotationsAttribute(constPool,AnnotationsAttribute.visibleTag);


        // 获取运行时的注解
        //AnnotationsAttribute attribute = (AnnotationsAttribute) classFile.getAttribute(AnnotationsAttribute.visibleTag);

        //AttributeInfo attribute1 = classFile.getAttribute(AnnotationsAttribute.visibleTag);

        attribute.addAnnotation(documentAnnotation);

        classFile.addAttribute(attribute);
        classFile.setVersionToJava5();

        // 打印修改后的注解
        javassist.bytecode.annotation.Annotation annotation = attribute.getAnnotation(Document.class.getName());

        MemberValue indexName1 = annotation.getMemberValue("indexName");
        System.out.println("修改后的值："+indexName1.toString());

        // 当前ClassLoader中必须尚未加载该实体。（同一个ClassLoader加载同一个类只会加载一次）
        EntityClassLoader loader = new EntityClassLoader(EopLogJt.class.getClassLoader());

        return ctClass.toClass(loader, null);
    }

    /*protected static class EntityClassLoader extends ClassLoader {
        private ClassLoader parent;

        public EntityClassLoader(ClassLoader parent) {
            this.parent = parent;
        }

        @Override
        public Class<?> loadClass(String name) throws ClassNotFoundException {
            return this.loadClass(name, false);
        }

        @Override
        protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
            Class<?> clazz = this.findLoadedClass(name);
            if (parent != null) {
                clazz = parent.loadClass(name);
            } else if (clazz == null) {
                this.findSystemClass(name);
            } else if (clazz == null) {
                throw new ClassNotFoundException();
            } else if (resolve) {
                this.resolveClass(clazz);
            }
            return clazz;
        }
    }*/
}
