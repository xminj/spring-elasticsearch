package com.elasticsearch.spring.springelasticsearch.annotation;

public class EntityClassLoader extends ClassLoader{
    private ClassLoader parent;

    public EntityClassLoader(ClassLoader parent) {
        this.parent = parent;
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        return this.loadClass(name, false);
    }

    @Override
    protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
        Class<?> clazz = this.findLoadedClass(name);
        if (parent != null) {
            clazz = parent.loadClass(name);
        } else if (clazz == null) {
            this.findSystemClass(name);
        } else if (clazz == null) {
            throw new ClassNotFoundException();
        } else if (resolve) {
            this.resolveClass(clazz);
        }
        return clazz;
    }

}
