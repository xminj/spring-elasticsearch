package com.elasticsearch.spring.springelasticsearch.repository;

import com.elasticsearch.spring.springelasticsearch.domain.EopLogJt;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface EopLogJtRepository  extends ElasticsearchRepository<EopLogJt,String> {

}
