package com.elasticsearch.spring.springelasticsearch;

import com.elasticsearch.spring.springelasticsearch.annotation.AnnotationFieldValue;
import com.elasticsearch.spring.springelasticsearch.domain.EopLogJt;
import com.elasticsearch.spring.springelasticsearch.domain.VersionDomain;
import com.elasticsearch.spring.springelasticsearch.repository.EopLogJtRepository;
import javassist.CannotCompileException;
import javassist.NotFoundException;
import org.elasticsearch.action.admin.indices.get.GetIndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.SearchQuery;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;

@SpringBootTest
class SpringElasticsearchApplicationTests {

    @Autowired
    private ElasticsearchTemplate template;
    @Autowired
    private Client client;

    @Autowired
    private EopLogJtRepository repository;


    @Test
    void contextLoads() {
        template.createIndex("first-index");
        Map<String, Object> setting = template.getSetting("first-index");
        System.out.println(setting);
    }


    @Test
    void setData() {
        VersionDomain domain = new VersionDomain()
                .setNumber("6.5.4")
                .setBuild_flavor("default")
                .setBuild_date("tar")
                .setBuild_hash("d2ef93d")
                .setBuild_date("2018-12-17T21:17:40.758843Z")
                .setBuild_snapshot("false")
                .setLucene_version("7.5.0")
                .setMinimum_index_compatibility_version("5.6.0")
                .setMinimum_wire_compatibility_version("5.6.0");


        Client client = template.getClient();
        IndexResponse string = client.prepareIndex("first-index", "string").setSource(domain, XContentType.JSON).get();
    }

    @Test
    void countLog() {
        //EopLogJt eopLogJt = new EopLogJt();
        for (int i = 0; i < 5; i++) {
            EopLogJt eopLogJt = new EopLogJt();
            repository.save(eopLogJt);
        }
    }


    @Test
    void queryIndex() throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException, NotFoundException, CannotCompileException {

        GetIndexRequest request = new GetIndexRequest();
        //request.addFeatures(GetIndexRequest.Feature.ALIASES);
        //IndicesAdminClient indices = client.admin().indices()
        IndicesAdminClient indices = template.getClient().admin().indices();
        String[] response = indices.getIndex(request).actionGet().getIndices();
        for (String str : response) {
            System.out.println(str);
        }

        boolean exists = template.indexExists("eop-log");
        //System.out.println(exists);
        if (exists) {

            //QueryBuilder queryBuilder = termQuery("","");
            SearchQuery query =
                    new NativeSearchQuery(QueryBuilders.matchQuery("apiId", "9193"));
            //AnnotationFieldValue.setAnnotationValue(clazz.getAnnotation(Document.class),"indexName","eop-log");

            Class<?> aClass = AnnotationFieldValue.documentIndexName("eop-log-2020.03.06",EopLogJt.class.getName());

            //Annotation[] annotations = aClass.getAnnotations();

            List<?> eopLogJts = template.queryForList(query, aClass);
            System.out.println(eopLogJts);




            /*SearchRequestBuilder srb = client.prepareSearch().setQuery(QueryBuilders.matchQuery("apiId","9193"));
            MultiSearchResponse items = client.prepareMultiSearch().add(srb).get();
            for (MultiSearchResponse.Item item :items.getResponses()){
                System.out.println(item.getFailureMessage());
            }*/
        }
        /*EopLogJt eopLogJt = new EopLogJt();
        EopLogJt index = repository.index(eopLogJt);
        Iterable<EopLogJt> all = repository.findAll();
        System.out.println(index);*/
    }
}
